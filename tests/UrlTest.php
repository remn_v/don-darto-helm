<?php

namespace DartoHelm\Test;

use DartoHelm\Utils\Url;

class UrlTest extends \PHPUnit_Framework_TestCase
{
    public function testAppendUrlNormal()   
    {
        $url = 'http://ahoy/site/muya';

        $info = [
            'token'=>'asdf'
        ];

        $this->assertEquals("{$url}?token=asdf", Url::append($url, $info));
    }

    public function testAppendUrlWithQ()
    {
        $url = 'http://ahoy/index.php?r=site%2Fpreauth';

        $info = [
            'token'=>'asdf'
        ];

        $this->assertEquals("{$url}&token=asdf", Url::append($url, $info));
    }

    public function testAppendUrlEmptyQ()
    {
        $url = 'http://ahoy/index.php?r=';

        $info = [
            'token'=>'asdf'
        ];

        $this->assertEquals("{$url}&token=asdf", Url::append($url, $info));
    }

    public function testAppendUrlInvalidQ()
    {
        $this->assertFalse(Url::isValid('http\{'));
        $this->assertTrue(Url::isValid('http://ahoy.net'));
    }
}
