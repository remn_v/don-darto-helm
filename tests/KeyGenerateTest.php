<?php

namespace DartoHelm\Test;

use \DartoHelm\App\Portal;
use \DartoHelm\App\Satellite;

class KeyGenerateTest extends \PHPUnit_Framework_TestCase 
{
    public function testGenPreauthKey()
    {
        $portal = new Portal([
            'source'=>'intranet',
            'preauthUrl'=>'http://localhost:8002/preauth',
            'preauthKey'=>'asdf',
        ]);

        $satApp = new Satellite([
            'intranet'=>[
                'ipaddr'=>['127.0.0.1', 'localhost'],
                'preauthKey'=>'asdf',
            ]
        ], 'qwerty');

        $user = [
            'uid'=>'7663',
            'username'=>'doni007'
        ];

        $this->assertTrue(
            $satApp->verifyRequestKey(
                [
                    'source'=>'intranet',
                    'user'=>$user
                ], 
                $portal->generateRequestKey($user)
            )
        );
    }

    public function testGenPreauthKeyOutOfSync()
    {
        $portal = new Portal([
            'preauthUrl'=>'http://localhost:8002/preauth',
            'preauthKey'=>'asdf',
        ]);

        $satApp = new Satellite([
            'intranet'=>[
                'ipaddr'=>['127.0.0.1', 'localhost'],
                'preauthKey'=>'asdf',
                'tokenSecret'=>'qwerty'
            ]
            
        ], 'qwerty');

        $user = [
            'uid'=>'7663',
            'username'=>'doni007'
        ];

        $this->assertFalse(
            $satApp->verifyRequestKey(
                [
                    'source'=>'intranet',
                    'user'=>$user
                ], 
                $portal->generateRequestKey($user, ((time() + 7) * 1000))
            )
        );
    }
}
