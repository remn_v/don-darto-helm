<?php

namespace DartoHelm\Test;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

use \DartoHelm\App\Portal;
use \DartoHelm\App\Satellite;

class PreauthTest extends \PHPUnit_Framework_TestCase 
{
    public function testSendRequest404()
    {
        $mock = new MockHandler([
            new Response(404, ['X-Error' => 'True'])
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $portal = new Portal([
            'source'=>'intranet',
            'preauthUrl'=>'http://localhost:8002/preauth',
            'preauthKey'=>'asdf',
        ], $client);
        
        $this->assertEquals($portal->sendServerPreauthRequest([
            'username'=>'doni007',
            'uid'=>'7663'
        ]), [
            'error'=>'Error code: 404'
        ]);
    }

    public function testSendRequest500()
    {
        $mock = new MockHandler([
            new Response(500, ['X-Error' => 'Server Error'])
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $portal = new Portal([
            'source'=>'intranet',
            'preauthUrl'=>'http://localhost:8002/preauth',
            'preauthKey'=>'asdf',
        ], $client);
        
        $this->assertEquals($portal->sendServerPreauthRequest([
            'username'=>'doni007',
            'uid'=>'7663'
        ]), [
            'error'=>'Error code: 500'
        ]);

    }

    public function testValidResponse()
    {
        $validResp = json_encode([
            'token'=>'ahoy123'
        ]);

        $mock = new MockHandler([
            new Response(200, ['Content-Type' => 'application/json'], $validResp)
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $portal = new Portal([
            'source'=>'intranet',
            'preauthUrl'=>'http://localhost:8002/preauth',
            'preauthKey'=>'asdf',
        ], $client);

        $result = $portal->sendServerPreauthRequest([
            'username'=>'doni007',
            'uid'=>'7663'
        ]);

        $this->assertEquals($result, [
            'success'=>true
        ]);
    }
}
