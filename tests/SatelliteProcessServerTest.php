<?php

namespace DartoHelm\Test;

use \Mockery;
use PHPUnit\Framework\TestCase;
use \DartoHelm\App\Portal;
use \DartoHelm\App\Satellite;
use Jasny\HttpMessage\ServerRequest;

class SatelliteProcessServerTest extends TestCase 
{
    public function testProcessPreauthEmpty()
    {
        $config = [
            'intranet'=>[
                'ipaddr'=>['127.0.0.1', 'localhost'],
                'preauthKey'=>'asdf',
            ]
        ];

        $satApp = new Satellite(
            $config,
            'qwerty',
            UserHandlerSample::class,
            SqlSample::class
        );

        //$stub = Mockery::mock('ServerRequest');
        //$stub->shouldReceive('getMethod')->andReturn('POST');
        //$stub->shouldReceive('getServerParams')->andReturn([
            //'REMOTE_ADDR'=>'127.0.0.1',
            //'REMOTE_HOST'=>'localhost'
        //]);
        //$stub->shouldReceive('getParsedBody')->andReturn();

        //$satApp->setServerHttpReq($stub);

        //$satApp->processServerPreauthReq();
    }

    public function testProcPreauthInvalid()
    {

    }

    public function testProcPreauthSucceed()
    {

    }
}


class UserHandlerSample extends \DartoHelm\Handler\User
{
    public function find($username)
    {
        return [
            'username'=>'doni007',
            'nama'=>'Donimaru Shinosuke',
            'uid'=>'1234'
        ];
    }

    public function setLogin($info)
    {
        return true;
    }
}

class SqlSample extends \DartoHelm\Utils\Sql
{
    public function execute($sql)
    {
        return [];
    }

    public function find($sql)
    {
        return [];
    }
}

