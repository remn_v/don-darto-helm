<?php

namespace DartoHelm\Test;

use DartoHelm\Utils\Sanitizer;

class SanitizerTest extends \PHPUnit_Framework_TestCase
{
    public function  testSha512Length()
    {
        $stringTest = '^*&asdfsdf';
        $this->assertFalse(Sanitizer::hashFormat('sha512', $stringTest));
    }

    public function testSha512Char()
    {
        $stringTest = 'e094ff5c66f8409ccffd624c7396750d15c4b1d7' 
            . 'b1f31aa1f0777330cec17488802fb9151c8166923' 
            . 'e98ebc255d090f03bbbc22dfbcbd6ee2fbfff10fef0%20;';

        $this->assertFalse(Sanitizer::hashFormat('sha512', $stringTest));
    }

    public function testSha512True()
    {
        $stringTest = hash_hmac('sha512', 'test123', 'ahoy');
        $this->assertTrue(Sanitizer::hashFormat('sha512', $stringTest));
    }

    public function testUsernameWrongChar()
    {
        $username = '\'ahoy004\'asdf';
        $this->assertFalse(Sanitizer::username($username));
    }

    public function testUsernameWrongLen()
    {
        $username = 'don';
        $this->assertFalse(Sanitizer::username($username));

        $username = 'donlex005';
        $this->assertFalse(Sanitizer::username($username));
    }

    public function testUsernameTrue()
    {
        $username = 'doni005';
        $this->assertTrue(Sanitizer::username($username));
    }
}
