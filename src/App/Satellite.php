<?php

namespace DartoHelm\App;

use DartoHelm\Base;
use DartoHelm\Handler\Token;
use Jasny\HttpMessage\ServerRequest;

class Satellite Extends Base
{
    private $userHandler;
    private $token;
    private $serverHttpReq;

    /**
     * Satellite constructor.
     * @param bool $cfg
     * @param bool $secret
     * @param bool $userHandlerClass
     * @param bool $sqlUtilsClass
     * @throws \Exception
     * @throws \Jasny\HttpMessage\RuntimeException
     */
    public function __construct($cfg = false, $secret = false,
            $userHandlerClass = false, $sqlUtilsClass = false)
    {
        parent::__construct($cfg);

        if ($userHandlerClass != false) {
            $this->userHandler = new $userHandlerClass();
        }

        if (!$secret) {
            throw new \Exception('no token secret set, please set one');
        }

        if ($sqlUtilsClass != false) {
            $this->token = new Token($secret, new $sqlUtilsClass());
        }

        $this->serverHttpReq = (new ServerRequest())->withGlobalEnvironment();
    }

    public function setServerHttpReq($req)
    {
        $this->serverHttpReq = $req;
    }

    /**
     * @param $req
     * @param $hash
     * @return bool
     * @throws \Exception
     */
    public function verifyRequestKey($req, $hash)
    {
        if (!array_key_exists('source', $req)) {
            throw new \Exception('invalid request source config');
        }

        $keyWindow = array_map(function($ts) use ($req) {
            $myTstamp =  (time() + $ts) * 1000;
            return $this->hashRequestKey(
                $req['user'], 
                $myTstamp, 
                $this->config[$req['source']]['preauthKey']
            );
        }, range(-5, 5));
        
        return in_array($hash, $keyWindow);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function processServerPreauthReq()
    {
        if ($this->serverHttpReq->getMethod() == 'POST') {
            $body = $this->serverHttpReq->getParsedBody();

            if (!$body) {
                return [
                    'error'=>'possibly empty/invalid body request'
                ];
            }

            if (!array_key_exists('source', $body)) {
                return [
                    'error'=>'Invalid source type'
                ];
            }

            $svrParams = $this->serverHttpReq->getServerParams();
            $sourceConfig = $this->config[$body['source']];

            if (!in_array($svrParams['REMOTE_ADDR'], $sourceConfig['ipaddr'])) {
                return [
                    'error'=>'Unauth ipaddr'
                ];
            }

            $dbUser = $this->userHandler->find($body['username']);

            if (!$dbUser) {
                return [
                    'error'=>'Invalid user'
                ];
            }
            
            $verified = $this->verifyRequestKey([
                'source'=>$body['source'],
                'user'=>[
                    'uid'=>$dbUser['uid'],
                    'username'=>$dbUser['username']
                ]
            ], $body['key']);

            if (!$verified) {
                return [
                    'error'=>'invalid preauth key'
                ];
            }

            return [
                'token'=>$this->token->create($dbUser['username'])
            ];
            
        } else {
            return [
                'error'=>'not a POST request'
            ];
        }
    }

    /**
     * @return array
     */
    public function verifyTokenRedirect()
    {
        if ($this->serverHttpReq->getMethod() == 'GET') {
            $qString = $this->serverHttpReq->getQueryParams();

            if (count($qString) <= 0) {
                return ['error'=>'invalid URI'];
            }

            if (!array_key_exists('token', $qString)) {
                return ['error'=>'invalid URI'];
            }

            $myToken = $this->token->find($qString['token']);

            if (empty($myToken)) {
                return ['error'=>'invalid token'];
            }

            if (!array_key_exists('token_key', $myToken)) {
                $myToken = $myToken[0];
            }

            $this->token->expires($qString['token']);
            $this->userHandler->setLogin($myToken['username']);

            return ['success'=>'token found'];
        } else {
            return ['error'=>'not a GET request'];
        }
    }
}
