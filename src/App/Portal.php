<?php

namespace DartoHelm\App;

use DartoHelm\Base;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;

use DartoHelm\Utils\Url;

class Portal extends Base
{
    /** @var string $token */
    private $token;

    /** @var HttpClient $http */
    private $http;

    public function __construct($cfg = false, $client = false)
    {
        parent::__construct($cfg);

        if (!$client) {
            $this->http = new HttpClient();
        } else {
            $this->http = $client;
        }
    }

    /**
     * @param $user
     * @param bool $tstamp
     * @return string
     * @throws \Exception
     */
    public function generateRequestKey($user, $tstamp = false)
    {
        return $this->hashRequestKey($user, $tstamp);
    }

    /**
     * @param $user
     * @return array
     * @throws \Exception
     */
    public function sendServerPreauthRequest($user)
    {
        if (!array_key_exists('username', $user)) {
            throw new \Exception('Username harus ada');
        }

        if (!array_key_exists('uid', $user)) {
            throw new \Exception('UID user harus ada');
        }
        
        try {
            $response = $this->http->request('POST', $this->config['preauthUrl'], [
                'json'=>[
                    'key'=>$this->generateRequestKey($user),
                    'username'=>$user['username'],
                    'source'=>$this->config['source']
                ]
            ]);
            
            $json = json_decode($response->getBody(), true);

            if (!$json) {
                return ['error'=>'invalid body response from satellite'];
            }

            if (!array_key_exists('token', $json)) {
                return ['error'=>'no token found in response message'];
            }

            $this->token = (string) $json['token'];

            return [
                'success'=>true
            ];
            
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return [
                    'error'=>'Error code: ' . $e->getResponse()->getStatusCode()
                ];
            } else {
                return [
                    'error'=>$e->getMessage()
                ];
            }
        }
    }

    public function retrieveToken() 
    {
        return $this->token;
    }

    /**
     * @param null|\Closure $fnRedirect
     * @throws \Exception
     * @return boolean
     */
    public function sendTokenRedirect($fnRedirect = null)
    {
        if (!$this->token) {
            throw new \Exception("invalid token redirect");
        }

        if (!Url::isValid($this->config['preauthUrl'])) {
            throw new \Exception('invalid preauth url config');
        }

        $redirectUrl = Url::append($this->config['preauthUrl'], [
            'token'=>$this->token
        ]);

        if (is_null($fnRedirect)) {
            return header("Location: " . $redirectUrl);
        } else {
            return $fnRedirect($redirectUrl);
        }
    }
}
