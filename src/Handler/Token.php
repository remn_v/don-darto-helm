<?php

namespace DartoHelm\Handler;

use DartoHelm\Utils\Sanitizer;

class Token
{
    private $secret;
    private $sql;

    public function __construct($scr = false, $sql = false)
    {
        if ($scr != false) {
            $this->secret = $scr;
        }

        if ($sql != false) {
            $this->sql = $sql;
        }
    }

    public function setSecret($scr)
    {
        $this->secret = $scr;
    }

    public function setSql($sql)
    {
        $this->sql = $sql;
    }

    public function create($username)
    {
        if (!Sanitizer::username($username)) {
            return false;
        }

        $newToken = $this->generate($username);
        $created = date_create('now')->format('Y-m-d H:i:s');
        $expires = date_create('now + 10 seconds')->format('Y-m-d H:i:s');

        $query = "INSERT INTO preauth_tokens(
            token_key, username, created, expires)
            VALUES ('{$newToken}', '{$username}', '{$created}', '{$expires}')";

        $res = $this->sql->execute($query);

        if (!$res) {
            return false;
        }

        return $newToken;
    }
    
    public function find($key)
    {
        if (!Sanitizer::hashFormat('sha512', $key)) {
            return false;
        }

        $now = date_create('now')->format('Y-m-d H:i:s');

        $query = "select * from preauth_tokens where 
            token_key='{$key}' and expires >= '{$now}'";

        return $this->sql->find($query);
    }

    public function expires($key)
    {
        if (!Sanitizer::hashFormat('sha512', $key)) {
            return false;
        }

        $search = $this->find($key);

        if (count($search) <= 0) {
            return false;
        }

        $query = "update preauth_tokens set expires='1970-01-01 00:00:00'";
        $res = $this->sql->execute($query);

        if ($res) {
            return false;
        }

        return true;
    }

    public function generate($username) 
    {
        if (!Sanitizer::username($username)) {
            throw new \Exception('invalid username format');
        }

        $tstamp = (time() * 1000) + rand();
        return hash('sha512', md5($this->secret) . '<|||>' . $tstamp . '?|||?' . $username);
    }
}
