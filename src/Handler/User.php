<?php

namespace DartoHelm\Handler;

abstract class User
{
    abstract public function find($username);
    abstract public function setLogin($username);
}
