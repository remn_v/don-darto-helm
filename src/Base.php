<?php

namespace DartoHelm;


class Base
{
    protected $config;

    public function __construct($cfg = false)
    {
        if ($cfg != false) {
            $this->config = $cfg;
        }
    }

    public function loadConfig($cfg)
    {
        $this->config = $cfg;
    }

    public function getTimestamp()
    {
        return time() * 1000;
    }


    public function hashRequestKey($user, $tstamp = false, $appKey = false)
    {
        if (!array_key_exists('username', $user)) {
            throw new \Exception('username param tidak ditemukan');
        }

        if (!array_key_exists('uid', $user)) {
            throw new \Exception('uid param tidak ditemukan');
        }

        if (!$tstamp) {
            $tstamp = $this->getTimestamp();
        }

        $username = $user['username'];
        $uid = $user['uid'];

        if (!$appKey) {
            $appKey = $this->config['preauthKey'];
        }

        return hash_hmac("sha512", 
            "||{$tstamp}^^{$username}^^{$uid}||", 
            $appKey
        );
    }
}
