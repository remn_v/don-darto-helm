<?php

namespace DartoHelm\Utils;

abstract class Sql
{
    abstract public function execute($sql);
    abstract public function find($sql);
}
