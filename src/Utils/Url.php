<?php 

namespace DartoHelm\Utils;

class Url
{
    public static function isValid($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        } 

        return true;
    }

    public static function append($strUrl, $info)
    {
        $url = parse_url($strUrl);;
        
        $fmrQ = $info;
        if (array_key_exists('query', $url)) {
            parse_str($url['query'], $parsedQ);
            $fmrQ = array_merge($parsedQ, $info);
        }

        return $url['scheme'] . '://' . $url['host'] . $url['path'] 
            . '?' . http_build_query($fmrQ);
    }
}
