<?php

namespace DartoHelm\Utils;

class Sanitizer
{
    const SHA512LEN = 128;

    public static function hashFormat($type, $content)
    {
        $passed = false;

        switch ($type) {
            case 'sha512':
                if (strlen($content) != self::SHA512LEN) {
                    break;
                }

                if (preg_match('/\A[a-f0-9]+\Z/m', $content)) {
                    $passed = true;
                    break;
                }

                break;

            default:
                $passed = false;
                break;
        }

        return $passed;
    }

    public static function username($str)
    {
        if (!preg_match('/\A[a-zA-Z0-9]{4,8}\Z/m', $str)) {
            return false;
        }

        return true;
    }
}
