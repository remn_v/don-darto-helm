# Darto Helm, a Preauth library for token authentication

Library Ini dapat digunakan untuk melakukan pengaturan single sign on antara portal aplikasi
dan aplikasi pendukung lainnya.


## Concept

Secara umum 5 alur proses dari proses token auth Darto Helm adalah sebagai berikut:

![concept](docs/img/concept-diagram.png)


Di bawah ini merupakan use case diagram, sequence diagram dan class diagram 
dari Darto Helm:


### Use Case Diagram

![use-case](docs/img/use-case-diagram.png)

### Sequence Diagram

![sequence](docs/img/sequence-diagram.png)

### Class Diagram

![sequence](docs/img/class-diagram.png)



## Setup

### Composer

Pertama-tama, tambahkan terlebih dahulu dependensi `darto-helm` pada `composer.json` 
direktori project:

```json
{
	"require": {
		"donixo/darto-helm": "dev-master"
	},
	"repositories": [
		{
			"type": "vcs",
			"url": "git@gitlab.dev.kominfo.go.id:donixo/darto-helm.git"
		}
	]
}
```

sebelumnya, pastikan bahwa SSH keys anda pada project sudah mendapatkan 
akses baca.


### Konfigurasi

#### Satellite

Aplikasi satelit dalam hal ini adalah aplikasi yang melakukan setup custom token 
dan menyetujui bahwa setiap user yang memiliki sesi dari aplikasi portal adalah 
user yang dapat secara layak untuk login ke dalam sistem.

Setelah melakukan update requirements di `composer.json`, selanjutnya dapat dibuat 
User dan SQL handler pada aplikasi yang dibutuhkan oleh `darto-helm` untuk 
dapat berjalan. Pada Yii, dapat dibuat pada folder `protected/components/`:

```bash
$ touch protected/components/DartoUserHandler.php
$ touch protected/components/DartoSqlHandler.php
```

Berikutnya, kita dapat mengisi baris kode pada konten tersebut:


```php
<?php 
class DartoUserHandler extends \DartoHelm\Handler\User 
{
    public function find($username)
    {
        //mengembalikan array user berdasarkan username 
        //dengan format sbb:
        //[
        //  'username'=>'myuser',
        //  'nama'=>'My User Fullname',
        //  'uid'=>'7542'
        //]
    }

    public function setLogin($info)
    {
        //memproses login pegawai spesifik ke proses bisnis
        //aplikasi satelit internal
    }
}
```

```php
<?php 
class DartoSqlUtils extends \DartoHelm\Utils\Sql 
{
    public function execute($sql)
    {
        //memproses query execute insert, update, delete
    }

    public function find($sql)
    {
        //memproses query select dari database
    }
    
}
```

Kemudian setelah selesai membuat dua komponen di atas, kita dapat melakukan pemasangan
pada action handler di controller. Berikut contoh action handler pada framework Yii1:

```php
<?php

public function actionPreauth()
{
	$sat = new \DartoHelm\App\Satellite(
		Yii::app()->params['preauth'],
		Yii::app()->params['preauth_secret'],
		DartoUserHandler::class,
		DartoSqlUtils::class
	);

	if (Yii::app()->request->isPostRequest) {
		header('Content-Type: application/json');
		echo json_encode($sat->processServerPreauthReq());
	} else {
		if (!Yii::app()->user->isGuest) { //logged in user
			Yii::app()->user->logout();
			unset(Yii::app()->session['tugas']); //other session info
		}

		$ret = $sat->verifyTokenRedirect();

		if (array_key_exists('error', $ret)) {
			echo "invalid token";
			return;
		}

		$this->redirect('/index.php/site/index');
	}
}

```

Contoh yii params di `config/main.php`:

```php
<?php
'preauth_secret'=>'preauth-secret-satellite-only',
'preauth'=>[
	'intranet'=>[
		'ipaddr'=>['127.0.0.1', '::1'],
		'preauthKey'=>'shared-key-with-portal'
	]
]
```



#### Portal

Pada portal, kita setidaknya harus menyediakan satu buah url untuk 
menghandle preauth request dan URL+token response. Contoh snippet di bawah ini
merupakan implementasi dari DartoHelm Portal pada Framework Yii 1:


Pada `config/main.php` params:

```php
<?php 
'preauth'=>[
	'sidara'=>[
		'preauthUrl'=>'http://sidara/index.php/site/preauth',
		'preauthKey'=>'sidara-key-preauth'
	],
	'eskp'=>[
		'preauthUrl'=>'http://eskp/index.php/site/preauth',
		'preauthKey'=>'esk-key-preauth'
	]
	'simpatik'=>[
		'preauthUrl'=>'http://simpatik/index.php/site/preauth',
		'preauthKey'=>'simpatik-key-preauth'
	]
]
```


Controller `site/mypreauth`:

```php
<?php 
 public function actionMypreauth($id)
{
	if (!Yii::app()->user->isGuest) {
		$satelliteList = ['sidara', 'eskp', 'simpatik'];
		
		if (!in_array($id, $satelliteList)) {
			return $this->redirect(array('site/cpanel'));
		}
		
		$dataUser = Yii::app()->user->DATAUSER;
		$myConf = Yii::app()->params['preauth'][$id];

		$portal = new DartoHelm\App\Portal([
			'source'=>'intranet',
			'preauthUrl'=>$myConf['preauthUrl'],
			'preauthKey'=>$myConf['preauthKey']
		]);

		$res = $portal->sendServerPreauthRequest([
			'username'=>Yii::app()->user->USERNAME,
			'uid'=>$dataUser->id
		]);
		
		if (!$res) {
			echo "invalid preauth response";
			return;
		}

		if (!array_key_exists('success', $res)) {
			echo "terjadi kesalahan sistem: " . $res['error'];
			return;
		}

		$portal->sendTokenRedirect();

	} else {
		$this->redirect(array('site/login'));
	}
}
```

Untuk framework yg menggunakan/turunan dari `symfony/http-foundation` seperti 
laravel 5.x,  dapat mengirimkan closure ke `sendTokenRedirect`:

```php
<?php 
... 

$portal->sendTokenRedirect(function ($url) {
    return redirect()->to($url);
});

```

